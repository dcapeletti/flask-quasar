from flask import Flask
from flask import render_template, jsonify, request
import requests
from flask_cors import CORS


app = Flask(__name__,
            static_url_path='/',
            static_folder="./dist/spa",
            template_folder="./dist/spa")

# app = Flask(__name__)
app.debug = False

cors = CORS(app, resources={r"/tarea/*": {"origins": "*"}})


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def capturar_todo(path):
    # print("En marcha...")
    if app.debug:
        print('Direccion: http://localhost:8080/{}'.format(path))
        return requests.get('http://localhost:8080/{}'.format(path)).text
    return render_template("index.html")


@app.route('/tarea', methods=["POST"])
def nuevaTarea():
	try:
		titulo = request.json['titulo']
		terminada = request.json['terminada']
		print('Se ha creado una nueva tarea. ', titulo, terminada)
		return jsonify('Tarea agregada en servidor...')
	except Exception:
		return jsonify(error='No se pudo agregar la tarea.'), 400

@app.route('/tarea/<string:nombre>', methods=['DELETE'])
def eliminarTarea(nombre):
	print('Eliminando tarea: ' + nombre)
	return jsonify('Eliminando tarea ' + nombre)


app.run(host='0.0.0.0')
